# Summary
The PHP Language role installs PHP to a server

This role is used to setup the following:

* Installs the application server and configures the server to be bonded to a green (internal) IP

# Variables
| Variable name           | Description                                                    | Variable File stored |
|-------------------------|----------------------------------------------------------------|----------------------|
